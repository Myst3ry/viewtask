package com.myst3ry.viewtask;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public final class TagsChipLayout extends ViewGroup {

    private int spacing = 10;

    public TagsChipLayout(Context context) {
        this(context, null);
    }

    public TagsChipLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TagsChipLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TagsChipLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initAttrs(context, attrs);
    }

    private void initAttrs(final Context context, final AttributeSet attrs) {
        final TypedArray ta = context.getTheme()
                .obtainStyledAttributes(attrs, R.styleable.TagsChipLayout_Layout, 0, 0);
        try {
            spacing = ta.getDimensionPixelSize(R.styleable.TagsChipLayout_Layout_spacing, spacing);
        } finally {
            ta.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int childCount = getChildCount();
        final int preMeasuredWidth = MeasureSpec.getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();

        int width = 0;
        int height = 0;
        int currentRowWidth = 0;

        for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() == View.GONE) {
                continue;
            }

            measureChild(child, widthMeasureSpec, heightMeasureSpec);

            width += Math.max(width, child.getMeasuredWidth() + spacing * 2);
            currentRowWidth += child.getMeasuredWidth() + spacing * 2;

            if (currentRowWidth > preMeasuredWidth) {
                height += child.getMeasuredHeight() + spacing * 2;
                currentRowWidth = child.getMeasuredWidth() + spacing * 2;
            } else {
                height = Math.max(height, child.getMeasuredHeight() + spacing * 2);
            }
        }

        height = Math.max(height, getSuggestedMinimumHeight());
        width = Math.max(width, getSuggestedMinimumWidth());

        setMeasuredDimension(resolveSize(width, widthMeasureSpec), resolveSize(height, heightMeasureSpec));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int childCount = getChildCount();

        final int layoutRight = getMeasuredWidth() - getPaddingRight();
        final int layoutLeft = getPaddingLeft();
        final int layoutTop = getPaddingTop();
        final int layoutWidth = layoutRight - getPaddingLeft();
        final int layoutHeight = getMeasuredHeight() - getPaddingBottom() - layoutTop;

        int currentHeight = layoutTop;
        int currentWidth = layoutRight;
        int childOnRow = 0;

        for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() == View.GONE) {
                continue;
            }

            child.measure(MeasureSpec.makeMeasureSpec(layoutWidth, MeasureSpec.AT_MOST),
                    MeasureSpec.makeMeasureSpec(layoutHeight, MeasureSpec.AT_MOST));

            final int width = child.getMeasuredWidth() + spacing * 2;
            final int height = child.getMeasuredHeight() + spacing * 2;

            if (currentWidth - width < layoutLeft) {
                currentWidth = layoutRight;
                currentHeight += height;
                childOnRow = 0;
            }

            int offset = 0;
            for (int j = 0; j <= childOnRow; j++) {
                final View lastChild = getChildAt(i - j);
                lastChild.layout(layoutRight - lastChild.getMeasuredWidth() - offset - spacing,
                        currentHeight + spacing,
                        layoutRight - offset - spacing,
                        currentHeight + lastChild.getMeasuredHeight() + spacing);
                offset += lastChild.getMeasuredWidth() + spacing * 2;
            }

            childOnRow++;
            currentWidth -= width;
        }
    }
}
