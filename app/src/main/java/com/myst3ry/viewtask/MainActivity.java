package com.myst3ry.viewtask;

import android.os.Bundle;
import android.support.design.chip.Chip;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public final class MainActivity extends AppCompatActivity {

    private TagsChipLayout containerOne;
    private TagsChipLayout containerTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        containerOne = findViewById(R.id.chips_container_one);
        containerTwo = findViewById(R.id.chips_container_two);

        final String[] stationsGreen = getResources().getStringArray(R.array.green_metro_stations_labels);
        final String[] stationsRed = getResources().getStringArray(R.array.red_metro_stations_labels);
        final String[] stationsPurple = getResources().getStringArray(R.array.purple_metro_stations_labels);
        final String[] stationsBlue = getResources().getStringArray(R.array.blue_metro_stations_labels);

        final List<Chip> stationsPackOne = new ArrayList<>();
        stationsPackOne.addAll(createChips(stationsGreen, R.color.colorGreen));
        stationsPackOne.addAll(createChips(stationsPurple, R.color.colorPurple));
        fill(containerOne, stationsPackOne);

        final List<Chip> stationsPackTwo = new ArrayList<>();
        stationsPackTwo.addAll(createChips(stationsRed, R.color.colorRed));
        stationsPackTwo.addAll(createChips(stationsBlue, R.color.colorBlue));
        fill(containerTwo, stationsPackTwo);
    }

    private void fill(final TagsChipLayout container, final List<Chip> chips) {
        for (final Chip chip : chips) {
            container.addView(chip);
            chip.setOnCloseIconClickListener(new OnChipClickListener());
        }
    }

    private List<Chip> createChips(final String[] labels, int colorRes) {
        final List<Chip> chips = new ArrayList<>(labels.length);
        for (final String label : labels) {
            final Chip chip = new Chip(this);

            chip.setId(View.generateViewId());
            chip.setClickable(false);

            chip.setText(label);
            chip.setTextColor(getResources().getColor(colorRes));

            chip.setCloseIconVisible(true);
            chip.setCloseIconResource(R.drawable.ic_close_24dp);
            chip.setCloseIconTintResource(colorRes);

            chip.setChipIconResource(R.drawable.ic_chip_circle);
            chip.setChipIconTintResource(colorRes);
            chip.setChipIconSizeResource(R.dimen.chip_icon_size);
            chip.setChipStartPaddingResource(R.dimen.chip_start_padding);

            chips.add(chip);
        }
        return chips;
    }

    private final class OnChipClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            if (view.getParent() == containerOne) {
                containerOne.removeView(view);
                containerTwo.addView(view);
            } else if (view.getParent() == containerTwo) {
                containerTwo.removeView(view);
                containerOne.addView(view);
            }
        }
    }
}
